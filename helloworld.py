from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app


import os
from google.appengine.ext.webapp import template
import sqlalchemy


db_user = os.environ.get("DB_USER")
db_pass = os.environ.get("DB_PASS")
db_name = os.environ.get("DB_NAME")
cloud_sql_connection_name = os.environ.get("CLOUD_SQL_CONNECTION_NAME")
db_url = os.environ.get("SQLALCHEMY_DATABASE_URI")


dbe = sqlalchemy.create_engine(
    db_url,
    pool_size=5,
    max_overflow=2,
    pool_timeout=30,
    pool_recycle=1800
    )


class MainPage(webapp.RequestHandler):
    
    
    def get(self):
        namevalues = []
        with dbe.connect() as conn:
            cursor = conn.execute(
            " select "
            " namevariable, valuevariable "
            " from sdb_namevariable nv "
            " join "
            " sdb_valuesvariable vv on nv.idnamevariable = vv.idnamevariable "
            " where vv.active_value = 1 ; "
            ).fetchall()

        for row in cursor:
            namevalues.append({
                'namevariable' : row[0],
                'valuevariable' : row[1]
            })

        template_values = {
            'list_variables': namevalues,
        }

        path = os.path.join(os.path.dirname(__file__), 'templates/index.html')
        self.response.out.write(template.render(path, template_values))


class SetPage(webapp.RequestHandler):
    
    
    def get(self):
        namevar = self.request.get('name')
        if namevar == "":
            self.response.out.write('It is need name variable!')
            return
        valuevar = self.request.get('value')
        if valuevar == "":
            self.response.out.write('It is need value variable')
            return
        namevalues = []
        with dbe.connect() as conn:
            conn.execute("begin transaction; ")
            conn.execute(
            "do $$ "                            
            "declare " 
            "curid int :=0; " 
            "curvalid int :=0; " 
            "begin "
            "select idnamevariable from sdb_namevariable where namevariable = '" + namevar + "' into curid; "
            "select idvaluesvariable from sdb_valuesvariable where idnamevariable = curid and valuevariable = '" + valuevar + "' into curvalid; "
            
            "if curid is not null and curvalid is null then " 
            "delete from sdb_commandlog where idnamevariable <> curid; "
            "delete from sdb_valuesvariable where idnamevariable <> curid and active_value = 0 ;"
            "update sdb_valuesvariable set active_value = 0 where idnamevariable = curid and active_value = 1;"                                                                   
            "insert into sdb_valuesvariable (idnamevariable, valuevariable, active_value) values (curid, '" + valuevar + "', 1); "
            "SELECT currval(pg_get_serial_sequence('sdb_valuesvariable', 'idvaluesvariable')) into curvalid; "
            "update sdb_commandlog set active_command = 0; "
            "insert into sdb_commandlog (idnamevariable, idvaluesvariable, active_command) values (curid, curvalid, 1); "
            "end if; "                                                                               
                         
            "if curid is null then "
            "insert into sdb_namevariable(namevariable) values('" + namevar + "'); "  
            "SELECT currval(pg_get_serial_sequence('sdb_namevariable', 'idnamevariable')) into curid; "
            "delete from sdb_commandlog where idnamevariable <> curid; "
            "delete from sdb_valuesvariable where idnamevariable <> curid and active_value = 0 ;"
            "insert into sdb_valuesvariable (idnamevariable, valuevariable, active_value) values (curid, '" + valuevar + "', 1); " 
            "SELECT currval(pg_get_serial_sequence('sdb_valuesvariable', 'idvaluesvariable')) into curvalid; "
            "update sdb_commandlog set active_command = 0; "
            "insert into sdb_commandlog (idnamevariable, idvaluesvariable, active_command) values (curid, curvalid, 1); "
            "end if; "         
            "end $$; ")
            conn.execute("commit transaction; ")
            cursor = conn.execute("select namevariable, valuevariable from sdb_namevariable "
            " join sdb_valuesvariable vv on sdb_namevariable.idnamevariable=vv.idnamevariable " +
            " where namevariable = '" + namevar + "'" 
            " and valuevariable = '" + valuevar + "';").fetchall()

        for row in cursor:
            namevalues.append({
                'namevariable' : row[0],
                'valuevariable' : row[1]
            })

        template_values = {
            'list_variables': namevalues,
        }

        path = os.path.join(os.path.dirname(__file__), 'templates/index.html')
        self.response.out.write(template.render(path, template_values))
        
        
class GetPage(webapp.RequestHandler):
    
    
    def get(self):
        namevar = self.request.get('name')
        if namevar == "":
            self.response.out.write('It is need name variable!')
            return
        namevalues = []
        with dbe.connect() as conn:
            cursor = conn.execute("select namevariable, valuevariable from sdb_namevariable "
            " join sdb_valuesvariable vv on sdb_namevariable.idnamevariable=vv.idnamevariable " +
            " where namevariable = '" + namevar + "'"
            " and active_value = 1;" ).fetchall()

        for row in cursor:
            namevalues.append({
                'namevariable' : row[0],
                'valuevariable' : row[1]
            })

        template_values = {
            'list_variables': namevalues,
        }

        path = os.path.join(os.path.dirname(__file__), 'templates/index.html')
        self.response.out.write(template.render(path, template_values))


class UnsetPage(webapp.RequestHandler):
    
    
    def get(self):
        namevar = self.request.get('name')
        if namevar == "":
            self.response.out.write('It is need name variable!')
            return
        namevalues = []
        with dbe.connect() as conn:
            conn.execute("begin transaction; ")
            conn.execute(
            "do $$ "                            
            "declare " 
            "curid int :=0; " 
            "curvalid int :=0; "
            "begin "
            "select idnamevariable from sdb_namevariable where namevariable = '" + namevar + "' into curid; "
            "select idvaluesvariable from sdb_valuesvariable where idnamevariable = curid and valuevariable is null into curvalid; "
            
            "if curid is not null and curvalid is null then " 
            "delete from sdb_commandlog where idnamevariable <> curid; "
            "delete from sdb_valuesvariable where idnamevariable <> curid and active_value = 0 ;"
            "update sdb_valuesvariable set active_value = 0 where idnamevariable = curid and active_value = 1;"                                                                   
            "insert into sdb_valuesvariable (idnamevariable, valuevariable, active_value) values (curid, null, 1); "
            "SELECT currval(pg_get_serial_sequence('sdb_valuesvariable', 'idvaluesvariable')) into curvalid; "
            "update sdb_commandlog set active_command = 0; "
            "insert into sdb_commandlog (idnamevariable, idvaluesvariable, active_command) values (curid, curvalid, 1); "
            "end if; "                                                                               
            "end $$; ")
            conn.execute("commit transaction; ")
            cursor = conn.execute("select namevariable, valuevariable from sdb_namevariable "
            " join sdb_valuesvariable vv on sdb_namevariable.idnamevariable=vv.idnamevariable " +
            " where namevariable = '" + namevar + "'" 
            " and active_value = 1 ;").fetchall()
                         
        for row in cursor:
            namevalues.append({
                'namevariable' : row[0],
                'valuevariable' : row[1]
            })

        if namevalues:
            template_values = {
                'list_variables': namevalues,
            }
            
            path = os.path.join(os.path.dirname(__file__), 'templates/index.html')
            self.response.out.write(template.render(path, template_values))
        else:
            self.response.out.write('There is not exist variable with name ' + namevar )
            

class NumequaltoPage(webapp.RequestHandler):
    
    
    def get(self):
        valuevar = self.request.get('value')
        if valuevar == "":
            self.response.out.write('It is need value variable!')
            return
        with dbe.connect() as conn:
            cursor = conn.execute(
            "select count(*) as countvariables from sdb_valuesvariable "
            "where valuevariable is not null and active_value =1 "
            "and valuevariable = '" + valuevar + "'; ").fetchall()
        countvariables = 0
        for row in cursor:
            countvariables = row[0]
            
        self.response.out.write('There is ' + str(countvariables) + ' variables with active value ' + valuevar )


class UndoPage(webapp.RequestHandler):
    
    
    def get(self):
        with dbe.connect() as conn:
            cursor = conn.execute(
            "select count(*) as cnt from sdb_commandlog where active_command = 0 and "
            " datetime < (select datetime from sdb_commandlog where active_command=1) ;"
            ).fetchall()
            
        cnt = 0
        for row in cursor:
            cnt = row[0]
        
        if cnt < 1:
            self.response.out.write('No commands for undo' )
        else:
            with dbe.connect() as conn:
                conn.execute("begin transaction; ")
                conn.execute(
                "do $$ "
                "declare "
                "    curdt timestamp; "
                "    idvv int; "
                "    idnv int; "
                "    idcl int; "
                "begin "
                "select datetime from sdb_commandlog where active_command = 1 into curdt; "
                "select idcomandlog from sdb_commandlog where active_command = 0  "
                "and datetime = (select max(datetime) from sdb_commandlog where active_command =0 "
                "and datetime < curdt) into idcl; "
                "select idvaluesvariable, idnamevariable from sdb_commandlog "
                "where idcomandlog = idcl into idvv, idnv; "
                "update sdb_valuesvariable set active_value = 0 where active_value = 1 and idnamevariable = idnv; "
                "update sdb_valuesvariable set active_value =1 where idvaluesvariable = idvv; "
                "update sdb_commandlog set active_command=0 where active_command = 1; "                
                "update sdb_commandlog set active_command=1 where idcomandlog = idcl; "                
                "end $$;"                
                )
                conn.execute("commit transaction; ")
                cursor = conn.execute(
                "select namevariable, valuevariable from sdb_namevariable "
                " join sdb_valuesvariable vv "
                "on sdb_namevariable.idnamevariable=vv.idnamevariable "
                "where vv.idvaluesvariable = (select idvaluesvariable from sdb_commandlog "
                "where active_command=1) ;"
                ).fetchall()
            namevalues = []
            for row in cursor:
                namevalues.append({
                    'namevariable' : row[0],
                    'valuevariable' : row[1]
                })
    
            template_values = {
                'list_variables': namevalues,
            }
    
            path = os.path.join(os.path.dirname(__file__), 'templates/index.html')
            self.response.out.write(template.render(path, template_values))


class RedoPage(webapp.RequestHandler):
    
    
    def get(self):
        with dbe.connect() as conn:
            cursor = conn.execute(
            "select count(*) as cnt from sdb_commandlog where active_command = 0 and "
            " datetime > (select datetime from sdb_commandlog where active_command=1) ;"
            ).fetchall()
            
        cnt = 0
        for row in cursor:
            cnt = row[0]
        
        if cnt < 1:
            self.response.out.write('No commands for redo' )
        else:
            with dbe.connect() as conn:
                conn.execute("begin transaction; ")
                conn.execute(
                "do $$ "
                "declare "
                "    curdt timestamp; "
                "    idvv int; "
                "    idnv int; "
                "    idcl int; "
                "begin "
                "select datetime from sdb_commandlog where active_command = 1 into curdt; "
                "select idcomandlog from sdb_commandlog where active_command = 0  "
                "and datetime = (select min(datetime) from sdb_commandlog where active_command =0 "
                "and datetime > curdt) into idcl; "
                "select idvaluesvariable, idnamevariable from sdb_commandlog "
                "where idcomandlog = idcl into idvv, idnv; "
                "update sdb_valuesvariable set active_value = 0 where active_value = 1 and idnamevariable = idnv; "
                "update sdb_valuesvariable set active_value =1 where idvaluesvariable = idvv; "
                "update sdb_commandlog set active_command=0 where active_command = 1; "                
                "update sdb_commandlog set active_command=1 where idcomandlog = idcl; "
                "end $$;"                
                )
                conn.execute("commit transaction; ")
                cursor = conn.execute(
                "select namevariable, valuevariable from sdb_namevariable "
                " join sdb_valuesvariable vv "
                "on sdb_namevariable.idnamevariable=vv.idnamevariable "
                "where vv.idvaluesvariable = (select idvaluesvariable from sdb_commandlog "
                "where active_command=1) ;"
                ).fetchall()
            namevalues = []
            for row in cursor:
                namevalues.append({
                    'namevariable' : row[0],
                    'valuevariable' : row[1]
                })
    
            template_values = {
                'list_variables': namevalues,
            }
    
            path = os.path.join(os.path.dirname(__file__), 'templates/index.html')
            self.response.out.write(template.render(path, template_values))


class EndPage(webapp.RequestHandler):
    
    
    def get(self):
        with dbe.connect() as conn:
            conn.execute("begin transaction; ")
            conn.execute(
            "do $$ "
            "begin "
            "delete from sdb_commandlog; "
            "delete from sdb_valuesvariable; "
            "delete from sdb_namevariable; "
            "end $$;"
            )
            conn.execute("commit transaction; ")
        self.response.out.write("Clean all data")


app = webapp.WSGIApplication([('/', MainPage)
                              , ('/set', SetPage)
                              , ('/get', GetPage)
                              , ('/unset', UnsetPage)
                              , ('/numequalto', NumequaltoPage)
                              , ('/undo', UndoPage)
                              , ('/redo', RedoPage)
                              , ('/end', EndPage)]
                              , debug=True)


def main():
    run_wsgi_app(app)

if __name__ == "__main__":
    main()
