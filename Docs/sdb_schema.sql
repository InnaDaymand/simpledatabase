CREATE TABLE "sdb_namevariable" (
"idnamevariable" serial2 NOT NULL,
"namevariable" varchar(150),
PRIMARY KEY ("idnamevariable") ,
CONSTRAINT "fk_sdb_namevariable_sdb_valuesvariable_1" FOREIGN KEY ("idnamevariable") REFERENCES "sdb_valuesvariable" ("idnamevariable")
)
WITHOUT OIDS;
COMMENT ON COLUMN "sdb_namevariable"."idnamevariable" IS 'Key of record';
COMMENT ON COLUMN "sdb_namevariable"."namevariable" IS 'name of variable';


CREATE TABLE "sdb_valuesvariable" (
"idvaluesvariable" serial2 NOT NULL,
"valuevariable" varchar(150),
"idnamevariable" int NOT NULL,
"active_value" int NOT NULL,
PRIMARY KEY ("idvaluesvariable") 
)
WITHOUT OIDS;
COMMENT ON COLUMN "sdb_valuesvariable"."idvaluesvariable" IS 'id of value variable';
COMMENT ON COLUMN "sdb_valuesvariable"."valuevariable" IS 'value of variable';
COMMENT ON COLUMN "sdb_valuesvariable"."idnamevariable" IS 'id of name variable';
COMMENT ON COLUMN "sdb_valuesvariable"."active_value" IS 'flag of active value';


CREATE TABLE "sdb_commandlog" (
"idcommandlog" serial2 NOT NULL,
"datetime" timestamp(0) NOT NULL DEFAULT current_timestamp,
"idnamevariable" int NOT NULL,
"idvaluesvariable" int NOT NULL,
"active_command" int NOT NULL,
PRIMARY KEY ("idcommandlog") ,
CONSTRAINT "fk_sdb_commandlog" FOREIGN KEY ("idnamevariable") REFERENCES "sdb_namevariable" ("idnamevariable") ON DELETE RESTRICT ON UPDATE RESTRICT,
CONSTRAINT "fk_sdb_commandlog_1" FOREIGN KEY ("idvaluesvariable") REFERENCES "sdb_valuesvariable" ("idvaluesvariable") ON DELETE RESTRICT ON UPDATE RESTRICT
)
WITHOUT OIDS;
COMMENT ON COLUMN "sdb_commandlog"."idcommandlog" IS 'id of command';
COMMENT ON COLUMN "sdb_commandlog"."datetime" IS 'date time of command';
COMMENT ON COLUMN "sdb_commandlog"."idnamevariable" IS 'id of name variable';
COMMENT ON COLUMN "sdb_commandlog"."idvaluesvariable" IS 'id of value variable';
COMMENT ON COLUMN "sdb_commandlog"."active_command" IS 'flag of active command';


